public class Ticket {

    private String nama;
    private String asal;
    private String tujuan;

    public Ticket(String nama, String asal, String tujuan) {
        this.nama = nama;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    public Ticket(String nama) {
        this.nama = nama;
    }

    /**
     * Menampilkan detail tiket.
     */
    public void detailTiket() {
        String info = "";
        if(asal == null && tujuan == null) {
            info += "Nama: " + this.nama;
        } else {
            info += "Nama\t: " + this.nama + "\n" + 
                    "Asal\t: " + this.asal + "\n" + 
                    "Tujuan\t: " + this.tujuan;
        }
        System.out.println(info);
        System.out.println("--------------------------------------------------|");
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String getNama() {
        return nama;
    }

    public String getAsal() {
        return asal;
    }

    public String getTujuan() {
        return tujuan;
    }
    
}