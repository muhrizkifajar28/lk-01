public class Kereta {

    private String namaKereta;
    private int jmlTiketTersedia;
    private Ticket[] tiket;

    /**
     * Constructor untuk kereta komuter.
     */
    public Kereta() {
        this.namaKereta = "Komuter";
        this.jmlTiketTersedia = 1000;
        tiket = new Ticket[this.jmlTiketTersedia];
    }

    /**
     * Constructor untuk kereta KAJJ.
     */
    public Kereta(String namaKereta, int jmlTiketTersedia) {
        this.namaKereta = namaKereta;
        this.jmlTiketTersedia = jmlTiketTersedia;
        tiket = new Ticket[this.jmlTiketTersedia];
    }
    
    /**
     * Method untuk menambahkan tiket (kereta komuter) ke dalam array Ticket[] tiket.
     * @param nama Nama dari pemilik tiket.
     */
    public void tambahTiket(String nama) {
        for (int i = 0; i < tiket.length; i++) {
            if (tiket[i] != null) {
                continue;
            } else {
                this.tiket[i] = new Ticket(nama);
                break;
            }
        }
        this.jmlTiketTersedia--;
        showMessage();
    }

    /**
     * Method untuk menambahkan tiket (kereta KAJJ) ke dalam array Ticket[] tiket.
     * @param nama Nama dari pemilik tiket
     * @param asal Asal dari pemilik tiket
     * @param tujuan Tujuan dari pemilik tiket
     */
    public void tambahTiket(String nama, String asal, String tujuan) {
        for (int i = 0; i < tiket.length; i++) {
            if (tiket[i] != null) {
                continue;
            } else {
                tiket[i] = new Ticket(nama, asal, tujuan);
                break;
            }
        } 
        this.jmlTiketTersedia--;
        showMessage();
    }

    /**
     * Method untuk menampilkan detail setiap tiket yang ada di dalam array Ticket[] tiket.
     */
    public void tampilkanTiket() {
        System.out.println("DAFTAR PENUMPANG KA "+this.namaKereta);
        System.out.println("===================================================");

        for (Ticket ticket : tiket) {  
            if (ticket == null) {
                break;
            } else {
                ticket.detailTiket();
            }
        }
    }

    /**
     * Method untuk menampilkan pesan notifikasi.
     */
    public void showMessage() {
        String pesan = "";
        if (jmlTiketTersedia >= 0) {
            pesan += "Tiket berhasil dipesan.";
        }else{
            pesan = "Tiket telah habis dipesan.\nSilahkan cari keberangkatan lainnya.";
        }
        //jika tiket kurang dari 30 maka akan ditampilkan sisa jumlah tiket.
        if (jmlTiketTersedia < 30 && jmlTiketTersedia >= 0) {
            pesan += " Jumlah tiket teresdia: " + jmlTiketTersedia;
        }
        System.out.println("+-------------------------------------------------+");
        System.out.println(pesan);
        System.out.println("+-------------------------------------------------+");
    }

    public String getNamaKereta() {
        return namaKereta;
    }

    public int getjmlTiketTersedia() {
        return jmlTiketTersedia;
    }

    public Ticket[] getTiket() {
        return tiket;
    }

    public void setNamaKereta(String namaKereta) {
        this.namaKereta = namaKereta;
    }

    public void setjmlTiketTersedia(int jmlTiketTersedia) {
        this.jmlTiketTersedia = jmlTiketTersedia;
    }

    public void setTiket(Ticket[] tiket) {
        this.tiket = tiket;
    }

}
    